﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    public class vm:viewmodelbase
    {
        enum Name : int 
        {
            First,
            Last
        }
        private string _FirstName;
        private string _LastName;
        private string _FullName;
        private string _Birth;

        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                if (!string.IsNullOrEmpty(value) && _FirstName != value)
                {
                    _FirstName = value;
                    Update();
                    //OnPropertyChanged("FirstName");
                }
            }
        }
        public string LastName
        {
            get { return _LastName; }
            set
            {
                if (!string.IsNullOrEmpty(value) && _LastName != value)
                {
                    _LastName = value;
                    Update();
                    //OnPropertyChanged("LastName");
                }
            }
        }
        public string Birth
        {
            get { return _Birth; }
            set 
            {
                if (!string.IsNullOrEmpty(value) && _Birth != value)
                {
                    _Birth = value;
                    Update();
                }
            }
 
        }
        public string FullName
        {
            get { return _FullName; }
            set
            {
                _FullName = value;
                //Update();
                OnPropertyChanged("FullName");
            }
        }
        private void Update()
        {
            int Age = CalculateBirth(Birth);
            FullName = FirstName + " " + LastName + " " + Convert.ToString(Age)+"Year Old";
            //string[] array;
            //array = FullName.Split(new string[] { " " }, StringSplitOptions.None);
            //FirstName = array[(int)Name.First];
            //LastName = array[(int)Name.Last];

        }
        public int CalculateBirth(string B)
        {
            int Age = 0;
            Age = 2015 - Convert.ToInt16(B);
            return Age;
        }
        public vm() { }
    }
}
